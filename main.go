// Datamerge merges velocity data from the DVL with depth data from the
// FOCUS vehicle to create aiding data for the Inertial Navigation
// System (INS).
package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
	"time"

	dvl "bitbucket.org/uwaploe/go-dvl"
	focus "bitbucket.org/uwaploe/go-focus"
	ins "bitbucket.org/uwaploe/go-ins"
	stan "github.com/nats-io/go-nats-streaming"
	"github.com/vmihailenco/msgpack"
)

const Usage = `Usage: datamerge [options]

Merge DVL and FOCUS data to create aiding data for the MUST INS.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	natsURL       string = "nats://localhost:4222"
	clusterID     string = "must-cluster"
	dvlSubject    string
	focusSubject  string
	aidingSubject string
)

type pressureValue struct {
	val float32
	m   sync.RWMutex
}

func (p *pressureValue) Pressure() float32 {
	p.m.RLock()
	defer p.m.RUnlock()
	return p.val
}

func (p *pressureValue) SetPressure(v float32) {
	p.m.Lock()
	defer p.m.Unlock()
	p.val = v
}

var badData uint64

func publishAidingData(sc stan.Conn, raw dvl.PD4, p *pressureValue, subj string) error {
	if raw.VelBtm[0] == dvl.BadVelocity {
		badData++
		if (badData % 100) == 0 {
			log.Printf("Discarded %d bad DVL data records", badData)
		}
		return nil
	}
	var rec ins.DvlData
	rec.T = time.Now().UTC()
	for i := 0; i < 3; i++ {
		rec.V[i] = float32(raw.VelBtm[i]) / float32(dvl.VelScale)
	}
	rec.Verr = float32(raw.VelBtm[3]) / float32(dvl.VelScale)
	rec.Pr = p.Pressure()
	b, err := msgpack.Marshal(&rec)
	if err != nil {
		return err
	}
	return sc.Publish(subj, b)
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&aidingSubject, "aid-sub", lookupEnvOrString("AIDING_SUBJECT", aidingSubject),
		"Subject name for INS aiding data")
	flag.StringVar(&dvlSubject, "dvl-sub", lookupEnvOrString("DVL_SUBJECT", dvlSubject),
		"Subject name for DVL data")
	flag.StringVar(&focusSubject, "focus-sub", lookupEnvOrString("FOCUS_SUBJECT", focusSubject),
		"Subject name for FOCUS data")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	parseCmdLine()

	sc, err := stan.Connect(clusterID, "data-merge", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	// Store the most recent pressure value by extracting the value from
	// the FOCUS data record.
	pv := &pressureValue{}
	focusCb := func(m *stan.Msg) {
		var sentence string
		// Accept tagged or untagged NMEA messages produced by nmeapub.
		bs := bytes.SplitN(m.Data, []byte(":"), 2)
		if len(bs) == 2 {
			sentence = string(bs[1])
		} else {
			sentence = string(bs[0])
		}
		rec, err := focus.ParseRecord(sentence)
		if err != nil {
			log.Printf("FOCUS data parse error: %v", err)
		} else {
			pv.SetPressure(rec["depth"])
		}
	}

	focusSub, err := sc.Subscribe(focusSubject, focusCb,
		stan.StartWithLastReceived())
	if err != nil {
		log.Fatalf("Cannot subscribe to FOCUS data: %v", err)
	}

	dvlCb := func(m *stan.Msg) {
		var rec dvl.PD4
		err := msgpack.Unmarshal(m.Data, &rec)
		if err != nil {
			log.Printf("Msgpack decode error: %v", err)
		} else {
			err = publishAidingData(sc, rec, pv, aidingSubject)
			if err != nil {
				log.Printf("Error publishing INS aiding data: %v", err)
			}
		}
	}

	dvlSub, err := sc.Subscribe(dvlSubject, dvlCb,
		stan.StartWithLastReceived())
	if err != nil {
		log.Fatalf("Cannot subscribe to DVL data: %v", err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	done := make(chan bool)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			dvlSub.Unsubscribe()
			focusSub.Unsubscribe()
			done <- true
		}
	}()

	log.Printf("Data merge service starting %s", Version)

	<-done
}
